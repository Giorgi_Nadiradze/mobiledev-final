### NoteKeeper
##### App made for simplifying keeping Notes
> Ui inspiration

![ui-inspiration](https://bitbucket.org/Giorgi_Nadiradze/mobiledev-final/raw/7ba3a8703d28b9eed7efde888ff0e041cd8bd282/ui-inspiration.png "ui-inspiration")


> Actual App layouts

![](https://bitbucket.org/Giorgi_Nadiradze/mobiledev-final/raw/7ba3a8703d28b9eed7efde888ff0e041cd8bd282/ui.png)

#### App consists of 3 activities:
- sign in activity
- sign up activity
- feed activity

------------
### Technical Review
- Sign In/Up system is Linked with Firebase
- Notes are saved in Firestore database, in document create with id matching user uid
- Note Feed is Made using RecyclerView with StaggeredGridLayout
- Notes are Synced with all devices using same user account

##### Used Resources
- ui inspiration from Dribble
- Fonts: [BPG Web 002 Caps](https://web-fonts.ge/bpg-web-002-caps/ "BPG Web 002 Caps") and [BPG Web 002](https://web-fonts.ge/bpg-web-002/ "BPG Web 002")

