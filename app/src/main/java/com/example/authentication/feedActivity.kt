package com.example.authentication

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.feed.*


class CustomAdapter(private val dataSet: ArrayList<String>?) :
    RecyclerView.Adapter<CustomAdapter.ViewHolder>() {

    /**
     * Provide a reference to the type of views that you are using
     * (custom ViewHolder).
     */
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val noteTextView: TextView

        init {
            // Define click listener for the ViewHolder's View.
            noteTextView = view.findViewById(R.id.note)
        }
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.note, viewGroup, false)

        return ViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        // Get element from your dataset at this position and replace the
        // contents of the view with that element
        viewHolder.noteTextView.text = dataSet?.get(position)
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount(): Int = dataSet?.size ?: 0

}

class feedActivity : AppCompatActivity() {
    var authInstace = FirebaseAuth.getInstance() // .currentUser?.uid.toString()
    var db =FirebaseFirestore.getInstance().collection("users").document( authInstace.currentUser?.uid.toString())
    var NOTES: ArrayList<String>? = null
    var recyclerViewAdapter: CustomAdapter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.feed)
          db
            .get()
            .addOnSuccessListener { document ->
                if (document.exists()) {
                    displayNotes(document.data as Map<String, ArrayList<String>>?)
                } else {
                    Log.d("custom", "No such document, Creating It")
                    db
                        .set(
                            hashMapOf(
                                "notes" to arrayListOf<String>("Hello, This Is your First Note")
                            )
                        )
                        .addOnSuccessListener { documentReference ->
                            displayNotes(
                                hashMapOf(
                                    "notes" to arrayListOf<String>("Hello, This Is your First Note")
                                )
                            )
                        }
                        .addOnFailureListener { e ->
                            Log.w(
                                "custom",
                                "Error adding document",
                                e
                            )
                        }
                }
            }
            .addOnFailureListener { exception ->
                Log.d("custom", "get failed with ", exception)
            }
        addNote.setOnClickListener {
            var newNote = noteEditText.text.toString();
            if (noteEditText.text.toString().isNotEmpty()) NOTES?.add(newNote)
            noteEditText.text?.clear()
            recyclerViewAdapter?.notifyItemInserted(NOTES?.size?.minus(1) ?: 0);
            db.update("notes", FieldValue.arrayUnion(newNote))
        }
    }

    private fun displayNotes(notes: Map<String, ArrayList<String>>?) {
        Log.d("custom", "${notes}")
        NOTES = notes?.get("notes")
        recyclerViewAdapter = CustomAdapter(NOTES)
        var layoutManager = StaggeredGridLayoutManager(
            2,
            StaggeredGridLayoutManager.VERTICAL
        )
        recyclerView.layoutManager = layoutManager
        recyclerView.setAdapter(recyclerViewAdapter)
    }

    override fun onDestroy() {
        super.onDestroy()
        authInstace.signOut()
    }

    override fun onStop() {
        super.onStop()
        authInstace.signOut()

    }


}